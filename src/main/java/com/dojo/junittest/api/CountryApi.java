package com.dojo.junittest.api;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dojo.junittest.service.CountryService;

@RestController
@RequestMapping("/country")
public class CountryApi {

    Logger log = Logger.getLogger(this.getClass().getName());

    private final CountryService service;

    public CountryApi(CountryService service) {
    	this.service = service;
    }

    @GetMapping("/countries")
    public ResponseEntity findAllCountries() {
        try {
            return ResponseEntity.ok(service.findAllCountries());
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("/codeByName")
    public ResponseEntity findCountryCodeByName(@RequestParam String country) {
        try {
            if (country.trim().isEmpty())
                return ResponseEntity.badRequest().body("El parametro ingresado no es valido");

            return ResponseEntity.ok(service.findCountryCodeByName(country));
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            return ResponseEntity.noContent().build();
        }
    }

}
