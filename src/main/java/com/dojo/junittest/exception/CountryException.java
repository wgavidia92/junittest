package com.dojo.junittest.exception;

public class CountryException extends Exception {

    public CountryException(String message) {
        super(message);
    }

}
