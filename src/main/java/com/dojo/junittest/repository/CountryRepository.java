package com.dojo.junittest.repository;

import com.dojo.junittest.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

    @Query("SELECT c.iso FROM Country c WHERE c.name LIKE %:countryName%")
    List<String> findCountyCodeByName(String countryName);

}
