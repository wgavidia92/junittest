package com.dojo.junittest.dto;

public record CountryDto(String name, String iso, String iso3) {
}
