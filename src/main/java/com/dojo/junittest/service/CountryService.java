package com.dojo.junittest.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dojo.junittest.dto.CountryDto;
import com.dojo.junittest.exception.CountryException;
import com.dojo.junittest.repository.CountryRepository;

@Service
public class CountryService {

    private final CountryRepository repo;

    public CountryService(CountryRepository repo) {
        this.repo = repo;
    }

    public List<CountryDto> findAllCountries() throws CountryException {
        var countries = repo.findAll();

        if (countries.isEmpty())
            throw new CountryException("No se encontraron registros de paises");

        return countries.stream()
                .map(c -> new CountryDto(c.getName(), c.getIso(), c.getIso3()))
                .toList();
    }

    public List<String> findCountryCodeByName(String countryName) throws CountryException {
        var codes = repo.findCountyCodeByName(countryName);

        if (codes.isEmpty())
            throw new CountryException("No se encontraron registros por el pais");

        return codes;
    }

}
