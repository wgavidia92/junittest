INSERT INTO COUNTRY(NAME, ISO, ISO3) VALUES ('COLOMBIA', 'CO', 'COL');
INSERT INTO COUNTRY(NAME, ISO, ISO3) VALUES ('CANADA', 'CA', 'CAN');
INSERT INTO COUNTRY(NAME, ISO, ISO3) VALUES ('UNITED STATES OF AMERICA', 'US', 'USA');
INSERT INTO COUNTRY(NAME, ISO, ISO3) VALUES ('ECUADOR', 'EC', 'ECU');
INSERT INTO COUNTRY(NAME, ISO, ISO3) VALUES ('BRAZIL', 'BR', 'BRZ');