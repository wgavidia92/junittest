# JUnit project


> Proyecto creado para demostración de pruebas unitarias con Junit.

### Requisitos
>
> * JDK21 o superior
> * IDE de preferencia (Eclipse, IntelliJ, Netbeans, VS Code)
> * Conocimiento básicos en Java
>


### Definición del proyecto

Proyecto creado con [spring initialzr](https://start.spring.io/) implementando las siguientes herramientas y librerias

- maven
- Base de datos en memoria H2
- Spring data JPA
- Spring web

El proyecto cuenta con dos endpoint encargados de retornar la siguiente información:

| Endpoint | Retorno |
| ------ | ------ |
| /country/countries | Lista: `[{country:(nombre del pais), iso: (código de dos digitos), iso3: (código de 3 digitos)}]` |
| /country/codeByName?country={country name} | Lista: `[{(Código del pais)}]` |

### Componentes y flujo

El proyect cuenta con 3 capas: Repository, Servicio y API, encargadas de: conectar a la base de datos, implementar reglas de negocio y exponer los servicios respectivamente. Estas capas se comunican de la siguiente manera:

![sequence](https://gitlab.com/wgavidia92/junittet/-/raw/main/sequence.png "Sequence diagram")


